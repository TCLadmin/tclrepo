repos:
	createrepo 7
	createrepo 6
	createrepo 5

deploy:
	rm -rf /srv/tclrepo/*
	cp -R 5 /srv/tclrepo
	cp -R 6 /srv/tclrepo
	cp -R 7 /srv/tclrepo
	cd /srv/tclrepo;\
		ln -s 7 7.4;\
        ln -s 7 7.5;\
        ln -s 7 7.6;\
        ln -s 7 7Server;\
        ln -s 7 7Workstation;\
        ln -s 6 6Workstation;\
        createrepo 7;\
	createrepo 6;\
	createrepo 5;\
        cd -
